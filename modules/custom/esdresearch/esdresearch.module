<?php

/**
 * File: esdresearch.module
 */

/**
 * Implements hook_config_info().
 */
function esdresearch_config_info() {
  $prefixes = array();
  $prefixes['esdresearch.settings'] = array(
    'label' => t('ESD Research configuration'),
    'group' => t('Configuration'),
  );
  return $prefixes;
}

/**
 * Implements hook_permission().
 */
function esdresearch_permission() {
  return array(
    'access all restricted results' => array(
      'title' => t('Access all restricted Results'),
      'description' => t('Access all restricted Results and Program Updates'),
    ),
  );
}

/**
 * general states:
 *  - own content is always visible
 *  - any content is visible with "any unpub" + "all restricted" permissions
 *  - unpublished unrestricted content is visible with "any unpublished" permission
 *  - published restricted content is visible with "all restricted" permission
 *
 * value dependent states:
 *  - published restricted content is visible by center role(s)
 *  - unpublished restricted content is visible by center role(s) + "any unpub"
 */
define('ESDRESEARCH_ANY', 1);
define('ESDRESEARCH_OPEN_UNPUB', 2);
define('ESDRESEARCH_RESTRICT_PUB', 3);
define('ESDRESEARCH_RESTRICT_UNPUB', 4);

/**
 * Implements hook_node_grants().
 */
function esdresearch_node_grants($account, $op) {
  $grants = &backdrop_static(__FUNCTION__);
  if (empty($grants) or !is_array($grants)) {
    $grants = array();
  }
  if (isset($grants[$op][$account->uid])) {
    return $grants[$op][$account->uid];
  }

  $grants[$op][$account->uid] = array();

  if ($op == 'view') {

    // owner always has access
    $grants[$op][$account->uid]['esdresearch_owner'] = array($account->uid);

    $all_restricted = user_access('access all restricted results', $account);
    $any_unpub = user_access('view any unpublished content', $account);
    if ($all_restricted and $any_unpub) {
      $grants[$op][$account->uid]['esdresearch_any'] = array(ESDRESEARCH_ANY);
    }
    else {
      $pub_gids = $unpub_gids = array();
      if ($all_restricted) {
        $pub_gids[] = ESDRESEARCH_RESTRICT_PUB;
      }
      if ($any_unpub) {
        $unpub_gids[] = ESDRESEARCH_OPEN_UNPUB;
      }
      $gids = _esdresearch_roles_allowed_gids();
      foreach ($account->roles as $role) {
        if (isset($gids[$role])) {
          $pub_gids[] = $gids[$role];
          if ($any_unpub) {
            $unpub_gids[] = $gids[$role];
          }
        }
      }
      if ($pub_gids) {
        $grants[$op][$account->uid]['esdresearch_pub'] = $pub_gids;
      }
      if ($unpub_gids) {
        $grants[$op][$account->uid]['esdresearch_unpub'] = $unpub_gids;
      }
    }
  }
  return $grants[$op][$account->uid];
}

/**
 * Implements hook_node_access_records().
 *
 *  Save as Draft creates unpublished nodes that people expect to be able to see,
 *    but Views, at least, requires a record in node_access, and core only adds
 *    a default record for published nodes. So we have to handle that in addition
 *    to restricted content.
 *
 *    @todo Do I have to create a default permission for published stuff myself now?
 */
function esdresearch_node_access_records($node) {
  $restricted = FALSE;
  if (isset($node->field_restrict_access) and isset($node->field_restricted_to)) {
    $wrapper = entity_metadata_wrapper('node', $node);
    $restricted = $wrapper->field_restrict_access->value();
  }
  if (!$restricted and $node->status) {
    // published unrestricted content - we have nothing to add
    return;
  }
  $grants = array();
  if (!empty($node->uid)) {
    // owner always has access to own nodes, regardless
    $grants[] = array(
      'realm' => 'esdresearch_owner',
      'gid' => $node->uid,
      'grant_view' => 1,
      'grant_update' => 0,
      'grant_delete' => 0,
      'priority' => 0,
    );
  }
  // for people who can see anything
  $grants[] = array(
    'realm' => 'esdresearch_any',
    'gid' => ESDRESEARCH_ANY,
    'grant_view' => 1,
    'grant_update' => 0,
    'grant_delete' => 0,
    'priority' => 0,
  );
  if ($restricted) {
    $allowed_roles = $wrapper->field_restricted_to->value();
    $gids = _esdresearch_roles_allowed_gids();
    if ($node->status) {
      // "all restricted" + published 
      $grants[] = array(
        'realm' => 'esdresearch_pub',
        'gid' => ESDRESEARCH_RESTRICT_PUB,
        'grant_view' => 1,
        'grant_update' => 0,
        'grant_delete' => 0,
        'priority' => 0,
      );
    }
    else {
      // "all restricted" + "any unpublished" is covered by ESDRESEARCH_ANY
    }
    foreach ($allowed_roles as $role) {
      if (isset($gids[$role])) {
        // per center restricted - published / unpublished
        $grants[] = array(
         'realm' => ($node->status ? 'esdresearch_pub' : 'esdresearch_unpub'),
         'gid' => $gids[$role],
         'grant_view' => 1,
         'grant_update' => 0,
         'grant_delete' => 0,
         'priority' => 0,
        );
      }
      else {
        error_log(__FUNCTION__ . ": no gid found for restricted_to value '{$role}' in {$node->type} node '{$node->nid}'");
      }
    }
  }
  elseif (empty($node->status)) {
    // unpublished unrestricted
    $grants[] = array(
      'realm' => 'esdresearch_unpub',
      'gid' => ESDRESEARCH_OPEN_UNPUB,
      'grant_view' => 1,
      'grant_update' => 0,
      'grant_delete' => 0,
      'priority' => 0,
    );
  }
  return $grants;
}

/**
 * Implements hook_node_presave()
 */
function esdresearch_node_presave(Node $node) {
  switch ($node->type) {
    case 'result':
      esdresearch_node_program_areas_notify($node);
      break;
  }
}

/**
 * Notify users about nodes based on Program Areas
 */
function esdresearch_node_program_areas_notify($node) {
  if (empty($node->status)) {
    // don't notify about unpublished content most other users can't see
    return;
  }
  $wrapper = entity_metadata_wrapper('node', $node);
  $program_areas = $wrapper->field_program_areas->value();
  $notified = $wrapper->field_notified->value();
  $tids = array();
  foreach ($program_areas as $term) {
    if (!empty($term->tid) && !empty($term->name)) {
      $tids[$term->tid] = $term->name;
    }
  }
  if (empty($tids)) {
    return;
  }
  // dpm(compact('node','program_areas','notified','tids'), __FUNCTION__);

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_program_areas', 'tid', array_keys($tids))
  ;
  // $query->addTag('efq_debug');
  $result = $query->execute();

  $accounts = array();
  if (!empty($result['user'])) {
    $accounts = entity_load('user', array_keys($result['user']));
  }
  if (empty($accounts[$GLOBALS['user']->uid])) {
    // $accounts[$GLOBALS['user']->uid] = user_load($GLOBALS['user']->uid);
  }
  if (empty($accounts)) {
    // nobody to notify
    return;
  }

  $params = array();

  $type = node_type_load($node->type);
  $params['subject'] = "Notification of New {$type->name} on NASA Earth Science Research Portal";

  $link = url("node/{$node->nid}", array('alias' => FALSE, 'absolute' => TRUE));
  $termnames = implode("\n", $tids);
  $params['message'] = <<<EOQ
A new {$type->name} is available on the NASA Earth Science Research Results Portal related to program areas of interest to you:

{$node->title}

{$link}

{$termnames}


EOQ;

  $now = time();
  $logtext = 'Notified @name about @type "%title"';
  $changed = FALSE;
  foreach ($accounts as $account) {
    if (!in_array($account->uid, $notified)) {
      $params['account'] = $account;
      backdrop_mail('esdresearch', 'program_areas_notify', $account->mail, user_preferred_language($account), $params);
      $notified[] = $account->uid;
      $logvars = array('@name' => $account->name, '@type' => $type->name, '%title' => $node->title);
      // backdrop_set_message(t($logtext, $logvars));
      watchdog('esdresearch', $logtext, $logvars, WATCHDOG_NOTICE, $link);
      $changed = TRUE;
    }
  }
  $node->field_notified[LANGUAGE_NONE] = array();
  foreach (array_filter($notified) as $i => $uid) {
    $node->field_notified[LANGUAGE_NONE][$i]['value'] = $uid;
  }
  // field_attach_update('node', $node);
}

/**
 * Implements hook_mail()
 */
function esdresearch_mail($key, &$message, $params) {
  $variables = array();
  $data = array('user' => $params['account']);
  $options = array('language' => $message['language']);
  user_mail_tokens($variables, $data, $options);

  switch ($key) {
    case 'program_areas_notify':
      $langcode = $message['language']->langcode;
      $message['subject'] = t($params['subject'], $variables, array('langcode' => $langcode));
      $message['body'][] = t($params['message'], $variables, array('langcode' => $langcode));
      break;
  }
}

/**
 * Implements hook_query_alter()
 *
 *  For Entity Query debugging
 */
function esdresearch_query_alter($query) {
  if ($query->hasTag('efq_debug')) {
    dpm((string)$query);
    dpm($query->arguments());
  }
}

/**
 * Implements hook_path_insert()
 */
function esdresearch_path_insert($path) {
  esdresearch_result_aliases('insert', $path);
}

/**
 * Implements hook_path_update()
 */
function esdresearch_path_update($path) {
  esdresearch_result_aliases('update', $path);
}

/**
 * Implements hook_path_delete()
 */
function esdresearch_path_delete($path) {
  esdresearch_result_aliases('delete', $path);
}

/**
 * Generate custom aliases
 */
function esdresearch_result_aliases($op = 'update', $path) {
  if (empty($path['source'])) {
    return;
  }
  $m = array();
  if (preg_match('#^node/([0-9]+)$#', $path['source'], $m)) {
    $nid = $m[1];
    $node = node_load($nid);
    if ($node and $node->type == 'result') {
      // error_log('update complete alias for result node ' . $nid);
      $options = array();
      $options['source'] = $path['source'] . '/complete';
      $options['alias'] = $path['alias'] . '/complete';
      // error_log('remove complete alias |' . $options['source'] . '| for result node ' . $nid);
      path_delete_all_by_source($options['source']);
      if ($op != 'delete') {
        // error_log('create complete alias |' . $options['alias'] . '| for result node ' . $nid);
        path_save($options);
      }
    }
  }
}


/**
 * Implements hook_preprocess_views_view().
 */
function esdresearch_preprocess_views_view(&$vars) {
  switch ($vars['display_id']) {
    case 'div_result_block':
      if (isset($vars['view']->result[0]->node_status) and empty($vars['view']->result[0]->node_status)) {
        $vars['classes'][] = 'unpublished';
      }
      foreach (array('[field_restrict_access]', '[field_sti_approval]') as $token_name) {
        if (!empty($vars['view']->style_plugin->render_tokens[0][$token_name])) {
          $vars['classes'][] = str_replace(' ', '-', $vars['view']->style_plugin->render_tokens[0][$token_name]);
        }
      }
      // fall through
    case 'result_page':
      backdrop_add_js(backdrop_get_path('module', 'esdresearch') . '/js/esdresearch.result_page.js');
      // fall through
    case 'result_block':
      if (empty($vars['rows'])) {
        // user shouldn't be here
        backdrop_access_denied();
        exit;
      }
      break;
  }
}

/**
 * Implements hook_preprocess_views_view_table().
 *
 *  Do this here and not in the theme so it affects the preview in Views UI.
 */
function esdresearch_preprocess_views_view_table(&$vars) {
  switch ($vars['view']->name) {
    case 'results':
    case 'program_updates':
      $unpub = FALSE;
      if (empty($vars['result'])) {
        break;
      }
      foreach ($vars['result'] as $i => $row) {
        if (!empty($row->nid)) {
          $node = node_load($row->nid);
          if (empty($node->status)) {
            // remove odd/even classes to let unpublished color show through
            $vars['row_classes'][$i] = array_diff($vars['row_classes'][$i], array('odd', 'even'));
            $vars['row_classes'][$i][] = 'unpublished';
          }
        }
      }
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for views exposed filter form
 *  For modifying the results-search view form
 */
function esdresearch_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
  if ($form_state['view']->name != 'results' or $form_state['view']->current_display != 'search_page') {
    return;
  }
  $selects = array(
    'field_pub_stage_value',
    'field_sources_tid',
    'field_centers_tid',
    'term_node_tid_depth',
    'field_newsworthiness_value',
    'field_programs_tid',
    'field_program_areas_tid',
  );
  foreach ($selects as $field_name) {
    if (isset($form[$field_name])) {
      $form[$field_name]['#chosen'] = FALSE;
      if (!empty($form[$field_name]['#multiple'])) {
        $form[$field_name]['#attributes']['size'] = min(12, count($form[$field_name]['#options']));
      }
    }
  }

  $form['keys']['#size'] = 60;

  // set descriptions of centers
  $center_terms = entity_load('taxonomy_term', array(), array('vocabulary' => 'centers'));
  foreach (array_keys($form['field_centers_tid']['#options']) as $k) {
    $v = $form['field_centers_tid']['#options'][$k];
    if (is_object($v)) {
      $tid = key($v->option);
      $label = current($v->option);
    }
    else {
      $tid = $k;
      $label = $v;
    }
    if (empty($label) or !is_numeric($tid)) {
      // All / empty
      continue;
    }
    if (isset($center_terms[$tid])) {
        $term = $center_terms[$tid];
        $wrapper = entity_metadata_wrapper('taxonomy_term', $term);
        $label = $wrapper->field_full_name->value();
    }
    else {
        $label = '<strong>' . var_export($label, true) . '</strong: (No such term ' . var_export($tid, true) . ' found)';
    }
    if (is_object($v)) {
      $form['field_centers_tid']['#options'][$k]->option = array($tid => $label);
    }
    else {
      $form['field_centers_tid']['#options'][$k] = $label;
    }
  }

}

/**
 * Implements hook_form_FORM_ID_alter() for result content type node form
 */
function esdresearch_form_result_node_form_alter(&$form, &$form_state, $form_id) {

  _esdresearch_field_restricted_to_states($form);

  if (empty($form['nid']['#value'])) {
    backdrop_set_title('Submit a Result');
    _esdresearch_default_contact($form);
  }

  // set descriptions of result types
  if (isset($form['field_result_type']['#language'])) {
    $langcode = $form['field_result_type']['#language'];
    $result_type_terms = entity_load('taxonomy_term', array(), array('vocabulary' => 'result_types'));
    foreach (array_keys($form['field_result_type'][$langcode]['#options']) as $tid) {
      $label = $form['field_result_type'][$langcode]['#options'][$tid];
      if (isset($result_type_terms[$tid])) {
        $term = $result_type_terms[$tid];
        $label = '<strong>' . $term->name . '</strong>: ' . preg_replace('#<[/]*p>#', '', $term->description);
      }
      else {
        $label = '<strong>' . $label . '</strong: (No such term ' . $tid . ' found)';
      }
      $form['field_result_type'][$langcode]['#options'][$tid] = $label;
    }
  }

  // set descriptions of centers
  if (isset($form['field_centers']['#language'])) {
    $langcode = $form['field_centers']['#language'];
    $center_terms = entity_load('taxonomy_term', array(), array('vocabulary' => 'centers'));
    foreach (array_keys($form['field_centers'][$langcode]['#options']) as $tid) {
      $label = $form['field_centers'][$langcode]['#options'][$tid];
      if (isset($center_terms[$tid])) {
        $term = $center_terms[$tid];
        $wrapper = entity_metadata_wrapper('taxonomy_term', $term);
        $label = $wrapper->field_full_name->value();
        if ($term->name == 'Other') {
          if (isset($form['field_centers_other'][$langcode][0]['value'])) {
            $form['field_centers_other'][$langcode][0]['value']['#states'] = array(
              'visible' => array(
                ':input[name="field_centers[' . $langcode . '][' . $tid . ']"]' => array(
                  'checked' => TRUE,
                )
              ),
            );
          }
        }
      }
      else {
        $label = '<strong>' . $label . '</strong: (No such term ' . $tid . ' found)';
      }
      $form['field_centers'][$langcode]['#options'][$tid] = $label;
    }
  }

  /**
   * We can set this to make all the date fields reveal themselves with progressive states:
   *  "published" shows all, "accepted" shows submission & acceptance
   * 'field_submission_date' => array('pending','accepted','published'),
   * 'field_acceptance_date' => array('accepted','published'),
   */

  $conditions = array(
    'field_submission_date' => array('pending'),
    'field_acceptance_date' => array('accepted'),
    'field_publication_date' => array('published'),
    'field_publication_ps' => array('published'),
  );

  foreach ($conditions as $field => $stages) {
    if (!isset($form[$field])) {
      error_log(__FUNCTION__ . ': no such publication field ' . $field);
      continue;
    }
    $langcode = $form[$field]['#language'];
    if (empty($form[$field][$langcode]['#type'])) {
      $form[$field][$langcode]['#type'] = 'container';
    }
    foreach ($stages as $stage) {
      $form[$field][$langcode]['#states']['visible'][':input[name="field_pub_stage[und]"]'][] = array('value' => $stage);
    }
  }

  // set visibility of source fields
  $source_fields = array(
    'field_sources_airborne' => 'Airborne (Instruments)',
    'field_sources_ground' => 'Ground-based',
    'field_sources_model' => 'Models',
    'field_sources_satellite' => 'Satellite (Instruments)',
  );
  if (isset($form['field_sources']['#language'])) {
    $langcode = $form['field_sources']['#language'];
    $source_types = array_flip($form['field_sources'][$langcode]['#options']);
    foreach ($source_fields as $field => $source) {
      if (!isset($form[$field]['#language'])) {
        error_log("No such source field '{$field}' in form");
      }
      elseif (!isset($source_types[$source])) {
        error_log("No such source type '{$source}' in field_sources options");
      }
      else {
        $slangcode = $form[$field]['#language'];
        if (empty($form[$field][$slangcode]['#type'])) {
          $form[$field][$slangcode]['#type'] = 'container';
        }
        $form[$field][$slangcode]['#states'] = array(
          'visible' => array(
            ':input[name="field_sources[' . $langcode . '][' . $source_types[$source] . ']"]' => array(
              'checked' => TRUE,
            ),
          ),
        );
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for update content type node form
 */
function esdresearch_form_update_node_form_alter(&$form, &$form_state, $form_id) {
  if (empty($form['nid']['#value'])) {
    backdrop_set_title('Submit a Program Update');
    _esdresearch_default_contact($form);
  }
  _esdresearch_field_restricted_to_states($form);
}

/**
 * Set default contact values in node form.
 */
function _esdresearch_default_contact(&$form) {
  $contact = array();
  $account = user_load($form['uid']['#value']);
  $contact['field_contact_name']['value'] = empty($account->field_complete_name[$account->langcode][0]['value']) ? '' : $account->field_complete_name[$account->langcode][0]['value'];
  $contact['field_contact_email']['email'] = $account->mail;

  foreach ($contact as $field => $default_value) {
    $langcode = empty($form[$field]['#language']) ? LANGUAGE_NONE : $form[$field]['#language'];
    $key = key($default_value);
    $value = current($default_value);
    if ($value and empty($form[$field][$langcode][0][$key]['#default_value'])) {
      $form[$field][$langcode][0][$key]['#default_value'] = $value;
    }
  }
}

/**
 * set Restricted To field visibility dependent on Restrict Access
 */
function _esdresearch_field_restricted_to_states(&$form) {
  if (isset($form['field_restrict_access']['#language'])) {
    $rtlangcode = $ralangcode = $form['field_restrict_access']['#language'];
    if (isset($form['field_restricted_to']['#language'])) {
      $rtlangcode = $form['field_restricted_to']['#language'];
    }
    $form['field_restricted_to'][$rtlangcode]['#states'] = array(
      'visible' => array(
        ':input[name="field_restrict_access[' . $ralangcode . ']"]' => array(
          'checked' => TRUE
        ),
      ),
    );
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter()
 *  for Paragraphs widget
 */
function esdresearch_field_widget_paragraphs_embed_form_alter(&$element, &$form_state, $context) {
  $max_delta = isset($element['#max_delta']) ? $element['#max_delta'] : 0;
  for ($delta = 0; $delta <= $max_delta; $delta++) {
    unset($element[$delta]['paragraph_bundle_title']);
  }
}

/**
 * Implements hook_field_attach_presave().
 */
function esdresearch_field_attach_presave($entity_type, $entity) {
  if ($entity_type == 'node') {
    if ($entity->type == 'result') {
      // Add parent locations
      if (!empty($entity->field_locations)) {
        foreach ($entity->field_locations as $langcode => &$deltas) {
          $new = array();
          $tids = array();
          foreach ($deltas as $delta => $item) {
            if (!empty($item['tid'])) {
              $tids[$item['tid']] = $item;
            }
          }
          foreach ($tids as $tid => $item) {
            $parents = taxonomy_term_load_parents_all($tid);
            foreach ($parents as $parent) {
              if (empty($tids[$parent->tid]) and empty($new[$parent->tid])) {
                $new[$parent->tid] = (array)$parent;
              }
            }
          }
          if ($new) {
            foreach ($new as $item) {
              $deltas[] = $item;
            }
          }
        }
      }
    }
  }
}

/**
 * Clean up unneeded help text in Paragraph field widget.
 *  https://github.com/backdrop-contrib/paragraphs/issues/27
 */
function esdresearch_paragraph_cleanup($output, $elements) {
  $output = preg_replace('#<p><em>No .*? added yet..*</em></p>#', '', $output);
  return $output;
}

/**
 * Maintain and return numeric keys for restricted_to role names
 */
function _esdresearch_roles_allowed_gids($role = NULL, $rebuild = FALSE) {
  $gids = &backdrop_static(__FUNCTION__);
  if (empty($gids) or !is_array($gids)) {
    $rebuild = TRUE;
  }
  elseif ($role and !isset($gids[$role])) {
    $rebuild = TRUE;
  }
  if ($rebuild) {
    $roles = _esdresearch_roles_allowed_values();
    $config = config_get('esdresearch.settings', 'restricted_gids');
    if (empty($config) or !is_array($config)) {
      $config = array();
    }
    $maxrid = $nextrid = empty($config) ? 10: max($config);
    foreach ($roles as $machine_name => $name) {
      if (!isset($config[$machine_name])) {
        $config[$machine_name] = ++$nextrid;
      }
    }
    if ($nextrid > $maxrid) {
      config_set('esdresearch.settings', 'restricted_gids', $config);
    }
    $gids = $config;
  }
  if ($role) {
    if (isset($gids[$role])) {
      return $gids[$role];
    }
    return FALSE;
  }
  return $gids;
}

/**
 * Return "internal_*" roles as options list
 * Updated to match Centers terms
 */
function _esdresearch_roles_allowed_values() {
  $options = array();
  $roles = array_map('check_plain', user_roles(TRUE));
  $tree = taxonomy_get_tree('centers');
  $centers = array();
  foreach ($tree as $term) {
    $centers[$term->name] = $term->name;
  }
  foreach ($roles as $machine_name => $name) {
    if (strpos($machine_name, 'internal_') === 0 or isset($centers[$name])) {
      $options[$machine_name] = $name;
    }
  }
  return $options;
}

/**
 * Implements hook_views_pre_execute()
 */
/* 
disable normally
function esdresearch_views_pre_execute(&$view) {
  if (isset($view->build_info['query'])) {
    // dpm(__FUNCTION__ . ' - the query');
    // dpq($view->build_info['query']);
  }
  else {
    // dpm($view, __FUNCTION__ . ' sees no query');
  }
}
*/

/**
 * Implements hook_preprocess().
 */
/*
disable this normally
function esdresearch_preprocess(&$vars, $hook) {
  static $hooks = array();
  if (!isset($hooks[$hook])) {
    // dpm($hook, __FUNCTION__);
    $hooks[$hook] = $hook;
  }
}
*/
