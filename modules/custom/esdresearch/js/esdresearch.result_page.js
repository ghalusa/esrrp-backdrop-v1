(function ($) {

  Backdrop.behaviors.resultPagePrintButton = {
    attach: function (context, settings) {
      $('button#result_page_print_button').click(function() {
          window.print();
        });
    }
  };

})(jQuery);
