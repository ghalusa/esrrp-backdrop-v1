ELEVATION
=========

This is a responsive theme based on the Elevation html5 theme by Pixelarity.


Installation
------------

- Install this theme using the official Backdrop CMS instructions at
  https://backdropcms.org/guide/themes

- There are no theme settings for this theme.

Issues
------

Bugs and Feature requests should be reported in the Issue Queue:
https://github.com/backdrop-contrib/elevation/issues

Current Maintainers
-------------------

- Robert Garrigos (https://github.com/robertgarrigos)
- Seeking additional maintainers

Credits
-------

- Original design by Pixelarity (https://pixelarity.com)

License
-------

This project is GPL v2 software. See the LICENSE.txt file in this directory for
complete text.
