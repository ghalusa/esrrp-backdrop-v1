<?php

/**
 * Implement custom version of theme_image_formatter()
 */
function esdr_bootstrap_image_formatter($vars) {
  $vars['path']['options']['attributes']['target'] = '_blank';
  return theme_image_formatter($vars);
}

/**
 * Implement custom version of theme_field() for field_doi
 */
function esdr_bootstrap_field__field_doi($vars) {
  if (!empty($vars['items'])) {
    foreach ($vars['items'] as $delta => &$row) {
      if (!empty($row['#markup'])) {
        $url = '';
        if (preg_match('/^http/', $row['#markup'])) {
          $url = $row['#markup'];
        }
        elseif (preg_match('/^doi:/', $row['#markup'])) {
          $url = str_replace('doi:', 'https://doi.org/', $row['#markup']);
        }
        elseif (preg_match('/^[0-9]+\.[0-9]+/', $row['#markup'])) {
          $url = 'https://doi.org/' . $row['#markup'];
        }
        if ($url) {
          $row['#markup'] = l($url, $url);
        }
      }
    }
  }
  return theme_field($vars);
}
