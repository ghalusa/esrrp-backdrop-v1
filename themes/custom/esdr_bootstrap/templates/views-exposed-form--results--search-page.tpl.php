<?php
// dpm($variables, __FILE__);
/**
 * @file
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */

$mytable = array(
  array(
    'ids' => array(
      'filter-keys',
    ),
    'suffix' => '<p><br/><strong style="text-decoration:underline;">Advanced Search Options</strong></p>',
  ),
  array(
    'prefix' => '<p>',
    'ids' => array(
      'filter-field_result_type_tid',
    ),
    'suffix' => '</p>',
  ),
  array(
    'prefix' => '<table border="0"><tr><td valign="top">',
    'ids' => array(
      'filter-name',
      'filter-field_centers_tid',
      'filter-field_pub_stage_value',
      'filter-field_sources_tid',
    ),
    'suffix' => '</td>',
  ),
  array(
    'prefix' => '<td valign="top">',
    'ids' => array(
      'filter-field_programs_tid',
      'filter-field_program_areas_tid',
      'filter-field_newsworthiness_value',
      'filter-field_locations_tid',
    ),
    'suffix' => '</td></tr></table>',
  ),
);
$mywidgets = array();
foreach ($mytable as $trow) {
    foreach ($trow['ids'] as $wid) {
      $mywidgets[$wid] = $widgets[$wid];
      unset($widgets[$wid]);
    }
}
$remains = array();
foreach (array_keys($widgets) as $wid) {
  $remains[] = $wid;
  $mywidgets[$wid] = $widgets[$wid];
  unset($widgets[$wid]);
}
$mytable[] = array('ids' => $remains);
?>
<?php if (!empty($q)): ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
<?php endif; ?>
<div class="views-exposed-form">
  <div class="views-exposed-widgets clearfix">
    <?php foreach ($mytable as $trow) : ?>
      <?php if (!empty($trow['prefix'])) print $trow['prefix']; ?>
      <?php if (!empty($trow['ids'])) : ?>
    <?php foreach ($trow['ids'] as $id) :?>
      <?php $widget = $mywidgets[$id]; ?>
      <div id="<?php print $widget->id; ?>-wrapper" class="views-exposed-widget views-widget-<?php print $id; ?>">
        <?php if (!empty($widget->label)): ?>
          <label for="<?php print $widget->id; ?>">
            <?php print $widget->label; ?>
          </label>
        <?php endif; ?>
        <?php if (!empty($widget->operator)): ?>
          <div class="views-operator">
            <?php print $widget->operator; ?>
          </div>
        <?php endif; ?>
        <div class="views-widget">
          <?php print $widget->widget; ?>
        </div>
        <?php if (!empty($widget->description)): ?>
          <div class="description">
            <?php print $widget->description; ?>
          </div>
        <?php endif; ?>
      </div>
    <?php endforeach; ?>
      <?php endif; ?>
      <?php if (!empty($trow['suffix'])) print $trow['suffix']; ?>
    <?php endforeach; ?>

    <?php if (!empty($sort_by)): ?>
      <div class="views-exposed-widget views-widget-sort-by">
        <?php print $sort_by; ?>
      </div>
      <div class="views-exposed-widget views-widget-sort-order">
        <?php print $sort_order; ?>
      </div>
    <?php endif; ?>
    <?php if (!empty($items_per_page)): ?>
      <div class="views-exposed-widget views-widget-per-page">
        <?php print $items_per_page; ?>
      </div>
    <?php endif; ?>
    <?php if (!empty($offset)): ?>
      <div class="views-exposed-widget views-widget-offset">
        <?php print $offset; ?>
      </div>
    <?php endif; ?>
    <div class="views-exposed-widget views-submit-button">
      <?php print $button; ?>
    </div>
    <?php if (!empty($reset_button)): ?>
      <div class="views-exposed-widget views-reset-button">
        <?php print $reset_button; ?>
      </div>
    <?php endif; ?>
  </div>
</div>
