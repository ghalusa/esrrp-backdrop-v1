<?php
/**
 * @file
 * Template for ESDR Flipped.
 *
 * Variables:
 * - $title: The page title, for use in the actual HTML content.
 * - $messages: Status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node.)
 * - $action_links: Array of actions local to the page, such as 'Add menu' on
 *   the menu administration interface.
 * - $classes: Array of CSS classes to be added to the layout wrapper.
 * - $attributes: Array of additional HTML attributes to be added to the layout
 *     wrapper. Flatten using backdrop_attributes().
 * - $content: An array of content, each item in the array is keyed to one
 *   region of the layout. This layout supports the following sections:
 *   - $content['header']
 *   - $content['full_banner']
 *   - $content['top_banner']
 *   - $content['top']
 *   - $content['sidebar']
 *   - $content['left']
 *   - $content['right']
 *   - $content['content']
 *   - $content['columna']
 *   - $content['columnb']
 *   - $content['tile1']
 *   - $content['tile2']
 *   - $content['tile3']
 *   - $content['footer']
 */
?>
<div class="esdr-flipped <?php print implode(' ', $classes); ?>"<?php print backdrop_attributes($attributes); ?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>

  <?php if (!empty($content['header'])): ?>
    <header id="l-header" class="l-header" role="header" aria-label="<?php print t('Site header'); ?>">
      <div class="container">
        <?php print $content['header']; ?>
      </div>
    </header>
  <?php endif; ?>

  <?php if (!empty($content['top_banner']) || !empty($content['full_banner'])): ?>
    <section class="l-top-banner">
      <div class="full-container">
        <?php print $content['full_banner']; ?>
      </div>
      <div class="container">
        <?php print $content['top_banner']; ?>
      </div>
    </section>
  <?php endif; ?>

  <?php if ($messages): ?>
    <section class="l-messages container">
      <?php print $messages; ?>
    </section>
  <?php endif; ?>

  <div class="l-main-wrapper">
    <main class="main container l-container" role="main">
      <div class="page-header">
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1 class="title" id="page-title">
            <?php print $title; ?>
          </h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
      </div>

      <?php if ($tabs): ?>
        <div class="tabs">
          <?php print $tabs; ?>
        </div>
      <?php endif; ?>

      <?php print $action_links; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 esdr-flipped-layouts-top panel-panel">
            <?php print $content['top']; ?>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3 esdr-flipped-layouts-sidebar panel-panel">
            <?php print $content['sidebar']; ?>
          </div>
          <div class="col-md-9 esdr-flipped-layouts-content panel-panel">
            <div class="row">
            <?php if ($content['left'] || $content['right']): ?>
              <div class="l-columns row">
                <div class="l-columns-region col-md-9">
                  <?php print $content['left']; ?>
                </div>
                <div class="l-columns-region col-md-3">
                  <?php print $content['right']; ?>
                </div>
              </div><!-- /.l-columns -->
            <?php endif; ?>
              <div class="row">
                <div class="col-md-12">
                  <?php print $content['content']; ?>
                </div>
              </div>
            <?php if ($content['columna'] || $content['columnb']): ?>
              <div class="l-columns row">
                <div class="l-columns-region col-md-8">
                  <?php print $content['columna']; ?>
                </div>
                <div class="l-columns-region col-md-4">
                  <?php print $content['columnb']; ?>
                </div>
              </div><!-- /.l-columns -->
            <?php endif; ?>
            <?php if ($content['tile1'] || $content['tile2'] || $content['tile3']): ?>
              <div class="l-tiles row">
                <div class="l-tiles-region col-md-4">
                  <?php print $content['tile1']; ?>
                </div>
                <div class="l-tiles-region col-md-4">
                  <?php print $content['tile2']; ?>
                </div>
                <div class="l-tiles-region col-md-4">
                  <?php print $content['tile3']; ?>
                </div>
              </div><!-- /.l-tiles -->
            <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>

  <?php if ($content['footer']): ?>
    <footer id="footer" class="l-footer" role="footer">
      <div class="container">
        <?php print $content['footer']; ?>
      </div>
    </footer>
  <?php endif; ?>
</div><!-- /.esdr-flipped -->
